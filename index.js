//use path module
const path = require('path');
//use express module
const express = require('express');
//use hbs view engine
const hbs = require('hbs');
//use bodyParser middleware
const bodyParser = require('body-parser');
//use mysql database
const mysql = require('mysql');
const app = express();

//Create Connection
const conn = mysql.createConnection({
  host     : '127.0.0.1',
  port     : '3306',
  user     : 'test',
  password : 'Test@123',
  database : 'shop_manager'
});

//connect to database
conn.connect((err) =>{
  if(err) throw err;
  console.log('Mysql Connected...');
});

//set views file
app.set('views',path.join(__dirname,'views'));
//set view engine
app.set('view engine', 'hbs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//set folder public as static folder for static file
app.use('/assets',express.static(__dirname + '/public'));

//route for homepage
app.get('/',(req, res) => {
  let sql = 'SELECT sh.shopID as shopID,sh.ShopName as shopName,ct.CatName as shopCategory,sh.ManName as shopManager,fl.floorName as shopLocation,sh.Desc as shopDesc FROM shop_manager.shop sh, shop_manager.Category ct, shop_manager.Floor fl WHERE sh.shopCatId=ct.idCategory and sh.shopFloorLoc=fl.idFloor;'
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.render('index',{
      results: results
    });
  });
});

//route for add a new shop
app.get('/addshop',(req, res) => {
  let sql = 'SELECT * FROM shop_manager.Category ct;'
  let sql1 = 'SELECT * FROM shop_manager.Floor;'
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    let query1 = conn.query(sql1, (err1, results1) => {
      if(err1) throw err1;
        res.render('addshop',{
        results: results,
        results1: results1
      });
    });
  });
});

//route for insert data
app.post('/save',(req, res) => {
  let data = {
              shopID: req.body.shopID,
              shopName: req.body.shopName,
              shopCatId: req.body.shopCatId,
              shopFloorLoc: req.body.shopFloorLoc,
              ManName: req.body.ManName,
              Desc: req.body.Desc
             };
  let sql = "INSERT INTO shop_manager.shop SET ?";
  let query = conn.query(sql, data,(err, results) => {
    if(err) throw err;
    res.redirect('/');
  });
});


//route for view shop data
app.post('/view',(req, res) => {
  let data={shopID: req.body.shopID}
  let sql0 = 'SELECT sh.shopID as shopID,sh.ShopName as shopName,sh.shopCatId as shopCatId,ct.CatName as shopCategory,sh.ManName as shopManager,sh.shopFloorLoc as shopFloorLoc, fl.floorName as shopLocation,sh.Desc as shopDesc FROM shop_manager.shop sh, shop_manager.Category ct, shop_manager.Floor fl WHERE sh.shopCatId=ct.idCategory and sh.shopFloorLoc=fl.idFloor and ?';
  let query = conn.query(sql0, data,(err, results) => {
    if(err) throw err;
    res.render('shopview',{
      results: results
    });

  });
});

//route for update data
app.post('/update',(req, res) => {
  let data1 = {
    shopName: req.body.shopName,
    shopCatId: req.body.shopCatId,
    shopFloorLoc: req.body.shopFloorLoc,
    ManName: req.body.ManName,
    Desc: req.body.Desc
   };
  let sql = "UPDATE shop_manager.shop SET ? WHERE shopID='"+req.body.shopIDup+"'";
  let query = conn.query(sql, data1,(err, results) => {
    if(err) throw err;
    console.log(query);
    res.redirect('/');
  });
});

//route for delete data
app.post('/delete',(req, res) => {
  let data={shopID: req.body.shopID};
  let sql = "DELETE FROM shop_manager.shop WHERE ?";
  let query = conn.query(sql, data,(err, results) => {
    if(err) throw err;
      res.redirect('/');
  });
});

//start the server

app.listen(process.env.PORT || 8080 ,function(){
  console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});
